package main

func main() {

	go recLiveUpdates()  // Get livestream frames
	go recDetections()   // Get still frames
	go liveSideChannel() // Send signal to cam telling that LS is open
	go serveLive()       // Serve livestream frames
	go serveStill()      //Serve up still images
	go detectLsSignal()  //Get data from webpage telling us that user is looking at it

}

func serveLive() {

}
