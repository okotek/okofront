module gitlab.com/okotek/okofront

go 1.16

require (
	gitlab.com/okotek/okoframe v0.0.0-20211210225943-ee2ad7b3d6eb // indirect
	gocv.io/x/gocv v0.29.0 // indirect
)
